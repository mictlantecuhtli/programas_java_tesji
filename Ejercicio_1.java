/*
* Fecha: 05_10_22
* Autor: Fátima Azucena MC
* Correo: fatimaazucenamartinez274@gmail.com
*/

import java.util.Scanner;

public class Ejercicio_1 {

	public static void main (String[] args){
	
		Scanner teclado = new Scanner (System.in);

		int anyos;
		int dias;
		int diasExtras;
		int menos;

		System.out.print("¿Llevas menos de un año laborando?: \n1)Si\n0)No\n");
		System.out.print("Digite su respuesta: ");
		menos = teclado.nextInt();

		if ( menos == 1 ){
		
			System.out.print("Lo siento, no tienes vacaciones\n");

		}
		else if ( menos == 0 ){
		
			System.out.print("¿Cuántos años llevas laborando?: ");
			anyos = teclado.nextInt();

			if ( anyos >= 1 && anyos <= 5 ) {
			
				System.out.print("Tiens 5 días de vacaciones\n");

			}
			else if ( anyos >= 5 && anyos <= 10 ) {
			
				System.out.print("Cuentas con 8 días de vacaiones\n");

			}
			else if ( anyos > 10 ) {
			
				dias = anyos - 10;
				diasExtras = 8 + dias;
				System.out.print("Cuentas con " + diasExtras + " días de vacaciones\n");

			}

		}


	}

}

