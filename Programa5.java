/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

import java.util.Scanner;

public class Programa5 {

	public static void main(String[] args){
	
		int suma = 0;
		int n;
		int a;
		Scanner teclado = new Scanner(System.in);

		System.out.print("Digite un número: ");
		n = teclado.nextInt();

		a = 1;

		while ( a <= n ){
		
			suma = suma + a;
			a++;
		
		}
			
		System.out.print("El resultado de la suma es: " + suma + "\n");	
	
	}


}
