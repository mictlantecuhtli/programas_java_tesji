/*
 * Nombre: Fátima Azucena MC
 * Fecha: 23_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

import java.util.Scanner;

public class Programa6 {

	public static void  main (String[] args){
	
		int a;
		int b;
		int resultado;
		Scanner teclado = new Scanner(System.in);

		a = 1;

		while ( a <= 15 ){
		
			b = 1;

			while ( b <= 15){
			
				resultado = a * b;
				System.out.print( a + " x " + b + " = " + resultado + " \n ");
				b++;
			
			}

			System.out.print("\n");
			a++;
		
		}
	
	}


}

