/*
* Autor: Fatima Azucena MC
* Correo: fatimaazucenamartinez274@gmail.com
* Fecha: 12_10_22
*/

import java.util.Scanner;

public class Ejercicio_2 {

	public static void main(String[] args) {
	
		Scanner teclado = new Scanner (System.in);
		int kilometros;
        	int carrera;
        	int lugar;
        	int a;
        	int b = 0;
        	int participacion;

		System.out.print("¿Participaste en la carrera?: \n");
		System.out.print("1)Si\n0)No\nDigite su respuesta: ");
		participacion = teclado.nextInt();

		if ( participacion == 0) {

                System.out.print("\nNo tienes ninguna decima");

        	}
        	else if ( participacion == 1 ) {

                	System.out.print("Tienes una decima extra por tu participación");
                	a = 1;
                	System.out.print("\n¿Cuántos kilometros recorriste?: ");
                	kilometros = teclado.nextInt();

                	if ( kilometros == 0 ) {

                        	System.out.print("\nTienes 0 decimas");

                	}

                	else if ( kilometros == 1 ) {

                        	b = a + 1;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

                	}
			else if ( kilometros == 2 ) {

                        	b = a + 2;
                        	System.out.print("\nTienes: "+ b +" decimas en total");


                	}

                	else if ( kilometros == 3 ) {

                        	b = a + 3;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

                	}

                	else if ( kilometros == 4 ) {

                        	b = a + 4;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

                	}
			else if ( kilometros == 5 ) {

                        	b = a + 5;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

                	}
                	else if ( kilometros == 6 ) {

                        	b = a + 6;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

               		}
                	else if ( kilometros == 7 ) {

                        	b = a + 7;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

                	}
                	else if ( kilometros == 8 ) {

                        	b = a + 8;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

                	}
			else if ( kilometros == 9 ) {

                        	b = a + 9;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

                	}
                	else if ( kilometros == 10 ) {

                        	b = a + 10;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

                	}
                	else if ( kilometros == 11 ) {

                        	b = a + 11;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

                	}
                	else if ( kilometros == 12 ) {

                        	b = a + 12;
                        	System.out.print("\nTienes: "+ b +" decimas en total");

                	}
			else if ( kilometros > 12 ) {

                        System.out.print("\nKilometros inválidos");

                	}

                	System.out.print("\n¿Quedaste en 1er, 2do o 3er lugar?: ");
                	System.out.print("\n1)Si\n0)No\nDigite su respuesta: ");
                	lugar = teclado.nextInt();

                	if ( lugar == 0 ) {

                        	System.out.print("Tienes "+ b + " decimas en total");

                	}

                	else if ( lugar == 1 ) {

                        	System.out.print("\nTienes " + b + " decimas\n");
				System.out.print("\nAdemás, excentaste la unidad 1 de Desarrollo\n");

                	}


        	}


	}


}
