/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

import java.util.Scanner;

public class Programa4 {

	public static void main(String[] args){
	
		int fact;
		int n;
		int a;
		int b;
		Scanner teclado = new Scanner(System.in);

		System.out.print("Digite hasta que numero desea ver su factorial: ");
		n = teclado.nextInt();

		fact = 1;
		a = 1;

		while ( a <= n ){
		
			b = fact * a;
			System.out.print(fact + " x " + a + " = " + b + "\n");
			fact = fact * a;
			a++;
		
		}
	
	}

}
