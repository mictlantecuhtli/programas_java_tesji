/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

import java.util.Scanner;

public class Programa3 {

	public static void main(String[] args){
		
		int tabla;
		int tope;
		int resultado;
		int a;
		Scanner teclado = new Scanner(System.in);

		System.out.print("¿Qué tabla desea ver?: ");
		tabla = teclado.nextInt();
		System.out.print("¿Hasta donde desea visualizar?: ");
		tope = teclado.nextInt();

		a = 1;

		while ( a <= tope ){
			
			resultado = tabla * a;		
			System.out.print(tabla + " x " + a + " = " + resultado + "\n");
			a++;

		}
	
	}

}
