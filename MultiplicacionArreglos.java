import java.util.Arrays;
import javax.swing.JOptionPane;

public class MultiplicacionArreglos {
    public static void main (String[] args){

        //Declaracion de variables
        byte a2 [] = new byte [5];
        byte a1 [] = new byte [5];
        short result [] = new short [5];
        byte i, y, a, b = 4;

        JOptionPane.showMessageDialog(null,"Los números ingresados deben de estar entre 0-100");
        System.out.print(" Arreglo 1 \n");
        for ( i = 0; i < 5; i++){
            a1[i] = Byte.parseByte(JOptionPane.showInputDialog("Ingrese el número en la posicion " + i));
            System.out.print(" " + a1[i] + " ");
        }
        System.out.print("\n\n Arreglo 2 \n");
        for ( y = 0; y < 5; y++){
            a2[y] = Byte.parseByte(JOptionPane.showInputDialog("Ingrese el número en la posicion " + y));
            System.out.print(" " + a2[y] + " ");
        }
        System.out.print("\n\n Resultado \n");
        for ( a = 0; a < 5; a++){
                result [a] = (short)( a1[a]*a2[b]);
                b--;
                System.out.print( " " + result[a] + " ");
        }
        //System.out.print(" " + (Arrays.toString(a1)));
    }
}

