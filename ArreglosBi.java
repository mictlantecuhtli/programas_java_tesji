import java.util.Arrays;
import javax.swing.JOptionPane;

public class ArregloBi {
    public static void main (String[] args){
    
        //Declaración de variables
        byte calificaciones [][] = new byte [6][5];
        String materia [] = {"Cálculo Diferencial","Fundamentos de programación","Química","Fundamentos de investigación",
        "Matemáticas Discretas","Desarrollo Sustentable"};
        String unidad [] = {"Unidad 1","Unidad 2","Unidad 3","Unidad 4","Unidad 5"};
        byte i;
        byte y;
        byte a;
        byte b;
        byte promedioFinal;
        short promedioUnidades = 0;
        
        for ( i = 0; i < 6; i++ ){
            short promedioMateria = 0;
            short sumaCalificaciones = 0;
            for( y = 0; y < 5; y++ ){
                calificaciones [i][y] = Byte.parseByte(JOptionPane.showInputDialog("Ingresa la calificacion de la " + unidad[y] + " de la materia " + materia[i]));
                sumaCalificaciones += calificaciones[i][y];
            }
            promedioMateria = ( short )(sumaCalificaciones / 5);
            promedioUnidades += ( short )( promedioMateria);
            System.out.print("El promedio general de la materia " + materia[i] + " es: " + promedioMateria + "\n");
        }
        promedioFinal = ( byte ) ( promedioUnidades / 6 );
        System.out.print("El promedio general es de: " + promedioFinal);
    }
}
