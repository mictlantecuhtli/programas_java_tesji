/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

import java.util.Scanner;

public class Programa2 {

	public static void main(String[] args){
	
		int n;
		String nombre;
		int a;
		Scanner teclado = new Scanner (System.in);

		System.out.print("Digita tu nombre completo: ");
		nombre = teclado.nextLine();
		System.out.print("Digite cuantas veces desea que se repita su nombre: ");
		n = teclado.nextInt();

		a = 1;

		while ( a <= n ) {
		
			System.out.print("Tu nombre es: " + nombre + "\n");
			a++;
		
		}
	
	}

}
