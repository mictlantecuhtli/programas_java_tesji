/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

import java.util.Scanner;

public class Programa1 {

	public static void main(String[] args) {		
		
		int a;
		int b;
		int resultado;
		Scanner teclado = new Scanner (System.in);

		System.out.print("Digite un valor: ");
		a = teclado.nextInt();
		System.out.print("Digite otro valor: ");
		b = teclado.nextInt();

		resultado = a + b;

		System.out.print("El resultado de la suma es: " + resultado + "\n");
	}

}
