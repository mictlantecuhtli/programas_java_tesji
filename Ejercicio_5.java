/*
 * Autor: Fatima Azucena MC
 * Fecha: 11_10_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

import javax.swing.JOptionPane;
import java.util.Scanner;

public class Ejercicio_5 {

	public static void main (String[] args){
		
		byte opcion;
		byte cantidad;
		short total = 0;
		Scanner teclado = new Scanner(System.in);

		JOptionPane.showMessageDialog(null,"Tallas disponibles:\n1.- Talla chica\n2.-Talla medidana\n3.-Talla grande");
		opcion = Byte.parseByte(JOptionPane.showInputDialog("\nDigite su opción: "));

		switch (opcion) {
			case 1:
				System.out.println("\nA elegido talla chica (Talla 7)\n¿Cuántos vestidos compro?: ");
				cantidad = teclado.nextByte();
				total = (short)(cantidad * 300);
			break;
			case 2:
				System.out.println("\nA elegido talla mediana (Talla 12)\n¿Cuántos vestidos compro?: ");
                                cantidad = teclado.nextByte();
                                total = (short)(cantidad * 400);
			break;
			case 3:
				System.out.println("\nA elegido talla grande (Talla 16)\n¿Cuántos vestidos compro?: ");
                                cantidad = teclado.nextByte();
                                total = (short)(cantidad * 500);
			break;
			default:
				System.out.println("\nOpción inválida");
		}

		System.out.println("\nEl total a pagar es: " + total);
	}

}
