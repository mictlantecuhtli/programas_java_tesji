/*
 * Autor: Fátima Azucena MC
 * Fecha: 20_10_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

import javax.swing.JOptionPane;
public class Ejercicio_4 {

	public static void main(String[] args){
	
		//Declaración de variables
		int precioTC = 300, precioTM = 400, precioTG = 500, total = 0, tC, tM, tG, opcion, talla, numV;

		 JOptionPane.showMessageDialog(null,"¿Compraste más de un tipo de vestido?\n1)Si\n0)No");
		 opcion = Integer.parseInt(JOptionPane.showInputDialog("Digite su opción: "));

		 if ( opcion == 1 ) {
		 
		 	tC = Integer.parseInt(JOptionPane.showInputDialog("¿Cuántos vestidos de talla chica compró?: "));
			tM = Integer.parseInt(JOptionPane.showInputDialog("¿Cuántos vestidos de talla mediana compró?: "));
       			tG = Integer.parseInt(JOptionPane.showInputDialog("¿Cuántos vestidos de talla grande compró?: "));
			total = (tC * precioTC) + (tM * precioTM) + (tG * precioTG);
			JOptionPane.showMessageDialog(null,"¿Compraste más de un vestido?\n1)Si\n0)No");			
		 }
		 else {
		 
			talla = Integer.parseInt(JOptionPane.showInputDialog("¿Qué talla es tu vestido?\n1)Chica\n2)Mediana\n3)Grande"));
			numV = Integer.parseInt(JOptionPane.showInputDialog("¿Cuántos vestidos fueron?: "));
			if ( talla == 1 ){
				total = numV * precioTC;
				JOptionPane.showMessageDialog(null,"El total a pagar es: "+ total  +" pesos");
			}
			else if ( talla == 2 ) {
				total = numV * precioTM;
				JOptionPane.showMessageDialog(null,"El total a pagar es: "+ total +" pesos");
			}
			else { 
				total = numV * precioTG;
				JOptionPane.showMessageDialog(null,"El total a pagar es: "+ total +" pesos");
			}
		 }
	}
}
