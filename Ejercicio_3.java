/*
 * Autor: Fátima Azucena MC
 * Fecha: 17_10_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

import javax.swing.JOptionPane;

public class Ejercicio_3 {

	public static void main(String[] args){
	
		int n;
		int d;
		int t;
		int m;

		n = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero (50-200)"));

		if ( n >= 50 && n <= 200 ) {
		
			d = (n*2);
			t = (d/3);
			m = (t/2);

			JOptionPane.showMessageDialog(null , " El doble de la tercera parte de la mitad de "+ n + " es: " + m );

		}
		else {
		
			JOptionPane.showMessageDialog(null, "Número inválido");

		}
	
	
	}

}
