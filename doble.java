/*
 * Autor: Fatima Azucena MC
 * Fecha: 09_09_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

public class doble {

	public static void main(String[] args){
	
		int numero, doble, tercera, mitad;

		numero = 30;
		doble = numero * 2;
		tercera = doble / 3;
		mitad = tercera / 2;

		System.out.print("La mitad de la tercera parte del doble de 30 es: " + mitad);
	
	}


}
