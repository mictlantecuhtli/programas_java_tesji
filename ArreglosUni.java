import javax.swing.JOptionPane;

public class ArreglosUni {
    public static void main(String[] args){

        //Declaración de variables
        byte arre1 [] = new byte [5];
        String titulos [ ]= {"Uni 1","Uni 2","Uni 3","Uni 4","Uni 5"};
        byte i;
        byte x;
        byte y;
        float promedio = 0;
        float promedioFinal;

        for ( i = 0; i < 5; i++ ){
            arre1[i] = Byte.parseByte(JOptionPane.showInputDialog("Ingrese la calificación de la " + titulos[i]));
            promedio += arre1[i];
        }
        promedioFinal = promedio / 5;
        for ( y = 0; y < 5; y++ ){
            JOptionPane.showInternalMessageDialog(null, "" + titulos[y]+ " " + arre1[y]);
        }
    }
}
